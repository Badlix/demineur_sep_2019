from tk_demineurseed import *
from tk_demineur_menu import *
import tk_dem_global    

def decouvrir_case():   
    y = 0
    x = 0
    while y < tk_dem_global.dimension and x < tk_dem_global.dimension:
        if tk_dem_global.grille[y][x] == 0 and tk_dem_global.case_decouverte[y][x] == 1:   
            if not y == 0 and not x == 0:
                tk_dem_global.case_decouverte[y-1][x-1] = 1
            if not y == 0:
                tk_dem_global.case_decouverte[y-1][x] = 1
            if not y == 0 and not x == tk_dem_global.dimension - 1:
                tk_dem_global.case_decouverte[y-1][x+1] = 1
            if not x == tk_dem_global.dimension - 1:
                tk_dem_global.case_decouverte[y][x+1] = 1
            if not y == tk_dem_global.dimension - 1 and not x == tk_dem_global.dimension - 1:
                tk_dem_global.case_decouverte[y+1][x+1] = 1
            if not y == tk_dem_global.dimension - 1:
                tk_dem_global.case_decouverte[y+1][x] = 1
            if not y == tk_dem_global.dimension - 1 and not x == 0:
                tk_dem_global.case_decouverte[y+1][x-1] = 1
            if not x == 0:
                tk_dem_global.case_decouverte[y][x-1] = 1
        if x == tk_dem_global.dimension - 1:
            x = 0
            y += 1
        else:
            x += 1
        
def case_visible_def():
    for repetition in range(tk_dem_global.dimension * 2):
        decouvrir_case()
