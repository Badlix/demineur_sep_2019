from random import *
from tk_demineur_menu import *
import tk_dem_global

def difficulte_choisie():
    creer_menu()
    if tk_dem_global.difficulte == 1:
        tk_dem_global.dimension = 10
        tk_dem_global.nb_mine = 10
    if tk_dem_global.difficulte == 2:
        tk_dem_global.dimension = 15
        tk_dem_global.nb_mine = 30
    if tk_dem_global.difficulte == 3:
        tk_dem_global.dimension = 20
        tk_dem_global.nb_mine = 50

def renit_grille():
    tk_dem_global.grille = []
    emplacement_mine = []
    mine = 0
    for num_ligne in range(tk_dem_global.dimension):
        ligne = [0] * tk_dem_global.dimension
        tk_dem_global.grille.append(ligne)

def creer_seed():
    mine = 0
    while tk_dem_global.nb_mine > mine:
        y = randint(0, tk_dem_global.dimension - 1)
        x = randint(0, tk_dem_global.dimension - 1)
        if tk_dem_global.grille[y][x] < 9:
            tk_dem_global.grille[y][x] = 9
            if not y == 0 and not x == 0:
                tk_dem_global.grille[y-1][x-1] += 1
            if not y == 0:
                tk_dem_global.grille[y-1][x] += 1
            if not y == 0 and not x == tk_dem_global.dimension - 1:
                tk_dem_global.grille[y-1][x+1] += 1
            if not x == tk_dem_global.dimension - 1:
                tk_dem_global.grille[y][x+1] += 1
            if not y == tk_dem_global.dimension - 1 and not x == tk_dem_global.dimension - 1:
                tk_dem_global.grille[y+1][x+1] += 1
            if not y == tk_dem_global.dimension - 1:
                tk_dem_global.grille[y+1][x] += 1
            if not y == tk_dem_global.dimension - 1 and not x == 0:
                tk_dem_global.grille[y+1][x-1] += 1
            if not x == 0:
                tk_dem_global.grille[y][x-1] += 1
            mine += 1

def creer_grille():
    difficulte_choisie()
    if tk_dem_global.continuer_de_jouer == 1:
        renit_grille()
        creer_seed()



