from Tkinter import *

fenetre = Tk()
dimension = 5

fenetre.grid()

grille = [[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]]
case_visible = [[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 1, 1, 2],[0, 0, 1, 1, 2],[0, 0, 1, 1, 2]]
boutons = []

def decouvre_bouton(x, y):
    print('{}, {}'.format(x, y))
    boutons[x][y].configure(text=grille[x][y])
            

# Initialisation du tableau de boutons
# On cree le tableau mais sans le remplir pour l'instant (utilisation de 'None')
for i in range(dimension):
    ligne = [None] * dimension
    boutons.append(ligne)


for i in range(dimension):
    for j in range (dimension):
        # tu avais raison pour lambda (en Python c'est un mot-cle, dans les autres langages c'est une notion)
        code_du_bouton = lambda y=i, x=j: decouvre_bouton(y, x)
        boutons[i][j] = Button(fenetre, height=1, width=1, command=code_du_bouton)
        # ton erreur etait de stocker le resultat de l'appel a 'grid'
        # la je prends soin de stocker le bouton dans la liste et ensuite j'appelle 'grid'
        boutons[i][j].grid(row=i, column=j)
        
# fenetre.bind('<Button 1>', motion)

fenetre.mainloop()