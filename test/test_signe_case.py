from tkinter import *

grille = [1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0]
case_visible = [0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0]
list_btn = []

root = Tk()
root.title("demineur")
root.grid()

def decouvrir_case2():   
    y = 0
    while y < 15:
        if grille[y] == 0 and case_visible[y] == 1:
            if not y == 0:
                case_visible[y-1] = 1
            if not y == 15 - 1:
                case_visible[y+1] = 1
        y += 1

for i in range(15):
    ligne = [None] 
    list_btn.append(ligne)

for i in range(15):
    list_btn[i] = Button(root, text='', height=1, width=1,command=lambda x=i: test(x))
    list_btn[i].grid(row=1, column=i)

def test(x):
    case_visible[x] = 1
    list_btn[x].config(text=grille[x])
    decouvrir_case2()
    affichage()

def affichage():
    for i in range(15):
        if case_visible[i] == 1:
            list_btn[i].config(text=grille[i])
        elif case_visible[i] == 0:
            list_btn[i].config(text='')

root.mainloop()
