from random import *

difficulte = int(input("CHOIX DE LA DIFFICULTE" + "\n" + "FACILE (5x5) = 1" + "\n" + "MOYEN (10x10) = 2" + "\n" + "DIFFICILE (15x15) = 3"))

if difficulte == 1:
    dimension = 5
    nb_mine = 3
elif difficulte == 2:
    dimension = 10
    nb_mine = 15
elif difficulte == 3:
    dimension = 15
    nb_mine = 35

grille = []
emplacement_mine = []
mine = 0
nb_ligne = dimension
nb_colonne = dimension

for num_ligne in range(nb_ligne):
    ligne = [0] * nb_colonne
    grille.append(ligne)

while nb_mine > mine:
    y = randint(0, dimension - 1)
    x = randint(0, dimension - 1)
    if grille[y][x] < 9:
        grille[y][x] = 9
        if not y == 0 and not x == 0:
            grille[y-1][x-1] += 1
        if not y == 0:
            grille[y-1][x] += 1
        if not y == 0 and not x == dimension - 1:
            grille[y-1][x+1] += 1
        if not x == dimension - 1:
            grille[y][x+1] += 1
        if not y == dimension - 1 and not x == dimension - 1:
            grille[y+1][x+1] += 1
        if not y == dimension - 1:
            grille[y+1][x] += 1
        if not y == dimension - 1 and not x == 0:
            grille[y+1][x-1] += 1
        if not x == 0:
            grille[y][x-1] += 1
        mine += 1







