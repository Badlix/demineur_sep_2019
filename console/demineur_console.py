from demineurseed import grille
from demineurseed import difficulte
from demineurseed import dimension
from demineurseed import mine
from demineur_case_visible import *

fin = 0
nb_drapeau_restant = mine
mine_a_trouver = mine - 1

def afficher_abcisse():
    abcisse_affichage = []
    abcisse_affichageAE = " A  B  C  D  E "
    abcisse_affichageFJ = " F  G  H  I  J "
    abcisse_affichageKO = " K  L  M  N  O "
    if difficulte > 0:
        abcisse_affichage.append(abcisse_affichageAE)
    if difficulte > 1:
        abcisse_affichage.append(abcisse_affichageFJ)
    if difficulte > 2:
        abcisse_affichage.append(abcisse_affichageKO)
    print(''.join(abcisse_affichage))

def nombre(nb):
    resultat = '[' + nb + ']'
    return resultat

def affichage_tableau(): 
    global fin
    y = 0
    x = 0
    affichage = []
    while y < dimension and x < dimension:
        if case_decouverte[y][x] == 1:
            aff_case_vide = str(grille[y][x])
            if int(grille[y][x]) > 8:
                fin = 1
                return fin 
        if case_decouverte[y][x] == 0:
            aff_case_vide = '-'
        if case_decouverte[y][x] == 2:
            aff_case_vide = 'D'
        affichage.append(nombre(aff_case_vide))
        if x == dimension - 1:
            x = 0
            y += 1
        else:
            x += 1
    afficher_abcisse()
    for i in range(dimension):
        print(''.join(affichage[dimension*i:dimension*(i+1)]))

def drapeau():
    global fin
    global mine_a_trouver
    global nb_drapeau_restant
    drap = raw_input("Voulez vous mettre=1/enlever=2 un drapeau ? rien faire=0")
    if not int(drap) == 0:
        drap_position = raw_input("En quelle position ?")
        drap_y = int(drap_position[1]) - 1
        drap_x = int(ord(drap_position[0]) - 65)
        if int(drap) == 1:
            nb_drapeau_restant -= 1
            case_decouverte[drap_y][drap_x] = 2
            if grille[drap_y][drap_x] > 8:
                if mine_a_trouver == 0:
                    fin = 2
                else:
                    mine_a_trouver -= 1
        if int(drap) == 2:
            nb_drapeau_restant += 1
            case_decouverte[drap_y][drap_x] = 0
            if grille[drap_y][drap_x] > 8:
                mine_a_trouver += 1
    print("Il reste " + str(nb_drapeau_restant) + " drapeau") 
    affichage_tableau()


while fin == 0:
    affichage_tableau()
    drapeau()
    if fin == 0:
        case_visible_def() #quest -> decouvrir
    if fin == 1:
        print("VOUS AVEZ EXPLOSER")
        break
    if fin == 2:
        print("VOUS AVEZ GAGNER")
        break




