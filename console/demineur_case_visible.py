from demineurseed import dimension
from demineurseed import grille


case_decouverte = []
for colonne in range(dimension):
    ligne = [0] * dimension
    case_decouverte.append(ligne)
  
def aff_case_decouverte():  
    for x in range(dimension):
        print(case_decouverte[x])

def decouvrir_case():
    case = raw_input("Quelle case voulez-vous decouvrir ?")
    casex = int(ord(case[0]) - 65)
    casey_for_nb_2_chiffre = int(case[1]) - 1
    casey = int(case[1]) - 1
    if len(case) > 2:
        casey = casey_for_nb_2_chiffre * 10 + int(case[2]) - 1
    case_decouverte[casey][casex] = 1

def decouvrir_case2():   
    y = 0
    x = 0
    while y < dimension and x < dimension:
        if grille[y][x] == 0 and case_decouverte[y][x] == 1:
            if not y == 0 and not x == 0:
                case_decouverte[y-1][x-1] = 1
            if not y == 0:
                case_decouverte[y-1][x] = 1
            if not y == 0 and not x == dimension - 1:
                case_decouverte[y-1][x+1] = 1
            if not x == dimension - 1:
                case_decouverte[y][x+1] = 1
            if not y == dimension - 1 and not x == dimension - 1:
                case_decouverte[y+1][x+1] = 1
            if not y == dimension - 1:
                case_decouverte[y+1][x] = 1
            if not y == dimension - 1 and not x == 0:
                case_decouverte[y+1][x-1] = 1
            if not x == 0:
                case_decouverte[y][x-1] = 1
        if x == dimension - 1:
            x = 0
            y += 1
        else:
            x += 1
        
def case_visible_def():
    decouvrir_case()
    for repetition in range(dimension * 2):
        decouvrir_case2()
